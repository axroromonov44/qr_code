import 'package:bar_code/bar_code/bar_code.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MaterialApp(
    title: '',
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Scanner'),
          centerTitle: true,
        ),
        body: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const BarCode(),
                    ),
                  ),
                  child: const Text(
                    'code scan',
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
